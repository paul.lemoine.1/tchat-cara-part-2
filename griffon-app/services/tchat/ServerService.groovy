package tchat

import griffon.core.artifact.GriffonService

@javax.inject.Singleton
@griffon.metadata.ArtifactProviderFor(GriffonService)
class ServerService {
	String message;
	String messageReceive;
	Socket connection;
	
	InputStream is
	InputStreamReader isr
	BufferedReader br
	OutputStream os
	OutputStreamWriter osw
	BufferedWriter bw
	
	List<String> listMessage;

	void enableConnection(def ip) {
		listMessage= new ArrayList()
		connection = new Socket(ip, 6788)
		receiveMessage()
	}

	String receiveMessage() {
		Thread t = new Thread() {
					public void run() {
						while (true){
							try{
								is = connection.getInputStream()
								isr = new InputStreamReader(is)
								br = new BufferedReader(isr)
								String message = br.readLine()
								System.out.println("Message received from the server : " +message)
								synchronized (listMessage) {
									listMessage.add(message);
								}
							}catch(IOException e ){
							}
						}
					}
				}
		t.start()
	}

	void sendMessage(String messageSend) {
		os = connection.getOutputStream()
		osw = new OutputStreamWriter(os)
		bw = new BufferedWriter(osw)

		String sendMessage = messageSend + "\n"
		bw.write(sendMessage)
		bw.flush()
		System.out.println("Message sent to the server : "+sendMessage)
	}

	boolean isEmpty() {
		return listMessage.empty
	}

	String getMessage() {
		return listMessage.remove(0)
	}

	void closeConnection() {
		try {
			br.close()
			isr.close()
			is.close()
			
			bw.close()
			osw.close()
			os.close()
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		connection.close()
	}
}
