application {
    title = 'app'
    startupGroups = ['app']
    autoShutdown = true
}
mvcGroups {
    // MVC Group for "app"
    'app' {
        model      = 'tchat.AppModel'
        view       = 'tchat.AppView'
        controller = 'tchat.AppController'
    }
	// MVC Group for "app"
	//TO-DO
}