package tchat

import javax.annotation.Nonnull

import org.codehaus.griffon.runtime.javafx.artifact.AbstractJavaFXGriffonView

import griffon.core.artifact.GriffonView
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor

@ArtifactProviderFor(GriffonView)
class TchatView extends AbstractJavaFXGriffonView {
	@MVCMember @Nonnull
	def builder
	@MVCMember @Nonnull
	def controller
	@MVCMember @Nonnull
	def model
	@MVCMember @Nonnull
	def parentView


	@Override
	public void initUI() {
		builder.with{
			//TO-DO
		}
	}
}
