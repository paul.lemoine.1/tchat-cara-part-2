package tchat

import java.util.logging.Logger

import javax.annotation.Nonnull

import griffon.core.artifact.GriffonController
import griffon.core.controller.ControllerAction
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor
import griffon.transform.Threading
import javafx.scene.control.Label

@ArtifactProviderFor(GriffonController)
class TchatController {
	@MVCMember @Nonnull
	def model
	@MVCMember @Nonnull
	def view
	
	@javax.inject.Inject
	private ServerService servService
	
	def cmptY
	def ip

	void mvcGroupInit(Map args) {
		//TO-DO
	}

	@Threading(Threading.Policy.OUTSIDE_UITHREAD)
	void enbableConnection(def ip){
		//TO-DO
	}

	@Threading(Threading.Policy.INSIDE_UITHREAD_ASYNC)
	void sendMessage() {
		//TO-DO
	}

	@Threading(Threading.Policy.OUTSIDE_UITHREAD_ASYNC)
	@ControllerAction
	void receiveMessage() {
		//TO-DO
	}

	@Threading(Threading.Policy.OUTSIDE_UITHREAD_ASYNC)
	void addMessage() {
		//TO-DO
	}
}
