package tchat

import griffon.core.artifact.GriffonController
import griffon.core.controller.ControllerAction
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor
import griffon.transform.Threading
import tchat.AppModel
import tchat.AppView

import javax.annotation.Nonnull

@ArtifactProviderFor(GriffonController)
class AppController{
    @MVCMember @Nonnull
    AppModel model
	@MVCMember @Nonnull
	AppView view

    @ControllerAction
    @Threading(Threading.Policy.INSIDE_UITHREAD_ASYNC)
    void launchTchat() {
		//TO-DO
    }
	
	@ControllerAction
	@Threading(Threading.Policy.INSIDE_UITHREAD_ASYNC)
	public void quit() {
		getApplication().shutdown()
	}
}