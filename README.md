# Partie 2 : Le MVC de tchat
- la correction de la partie 1 est présente dans cette même partie n'hésitez pas à y jeter un œil !
- Une fois la première partie faite, nous allons nous occuper de la partie chat de l'application.
- Avant de démarrer, nous avons mis a disposition le serveur permettant la communication des messages :
	- lien à cloner : https://gitlab.com/paul.lemoine.1/serveur_tchat_cara.git
	- A importer dans Eclipse comme un simple projet java.
- A la fin de cette partie vous arriverez a ce résultat :

![alt text](https://imageshack.com/a/img921/9729/Wwk9EN.png  "Resultat partie 2")

# 1. Configuration du fichier Config.groovy
- Les classes dont vous avez besoin pour réaliser cette partie son déjà présentes.
- Il vous faut juste déclarer le nom 'tchat' du MVCGroup ainsi que les classes associées.
	- Nous avons :
		- le TchatController
		- le TchatModel
		- le TchatView

# 2.  Le TchatView :
- Dans cette partie vous devez créer la vue du tchat (pour vous aider vous pouvez vous baser sur l'AppView)
- Notre view se compose de :
	-  le builder
		- la scene : 
			- id: tchat
			- une hbox
				- un group
					- un gridPane: 
						- id: gridMenuTchat
						- Un scrollPane
							- Un GridPane
								- id : gridTchat
								- un scale (x:2,y:2) (pour le grossissement des messages)
					- un gridPane stocker dans une variable gridMenu
						- id: gridMenuSend
						- un textField
							- id : textSend
						- un button
							- id : Envoyer 
							- méthode du controller : sendMessage()
							
-  Associez le gridMenu au controller.
-  Associez le texte du bouton Envoyer.
- Faire le changement de vue en plaçant la scène du builder de la view dans le scène du builder de sa parentView.
- 
# 3. Le TchatModel:
- Dans cette partie nous allons juste définir ce que doit stocker la vue, ici nous avons besoin de :
	- le pseudo de l'utilisateur (donné par l'AppView).
	- Un message représentant le message que l'on va envoyer.

- Une fois cela fait, retouner dans la vue est "binder" le message sur le textField de la vue.

# 4. Le TchatController :

- Dans cette partie nous allons nous occuper de la gestion des messages ainsi que la communications avec le service de tchat.
- Dans la méthode, le service est déjà injecté ainsi que les variables dont vous aurez besoin sont définis.

## 4.a Les méthodes :

- Pour faciliter l'implémentation des méthodes un service permettant la liaison avec le serveur vous est donné.
- jetez y un oeil pour savoir quelles fonctions sont intéressantes ! :) 

### 4.a.1 Implémentation de la méthode MVCGroupInit()
- Dans cette méthode nous allons chercher à récupérer le pseudo donné par l'AppView et l'affecter à notre model. Puis nous allons lancer la connection entre notre serveur et notre application et enfin lancer le thread de récupération de messages:

	- Initialisation des variables: 
		- La récupération du pseudo se fait via le paramètre  :Map args.
		- Ensuite initialisez la variable ip à localhost
		- Le compteurY à 0
	- lancement des méthodes :
		- appelez la méthode enableConnection qui prend en paramètre l'ip
		- et la méthode receiveMessage qui permet la récupération des messages.

### 4.a.1 Implémentation de la méthode enableConnection()

- Pour activer la connexion, appelez le service sur la méthode appropriée.

### 4.a.2 Implémentation de la méthode sendMessage()

- Dans notre vue cette méthode est a "binder" sur le bouton Envoyer.
- Elle récupère le message et le pseudo de notre model
- et le donne à notre service via la méthode associée
- pour terminer il faut reset notre textField en passant par le builder de notre view

### 4.a.3 Implémentation de la méthode receiveMessage()
- Appelez le service sur la méthode appropriée
- lancez la méthode permettant l'ajout de message addMessage()

### 4.a.4 Implémentation de la méthode addMessage()
- Cette méthode se base sur un thread qui est en "dehors" de notre UI (caractérisé par la threading.policy)
- Cependant pour son bon fonctionnement, celle-ci se découpe en deux parties, une partie en dehors de notre UI et une dedans.
	- Cette gestion est faite via Griffon par les méthodes : 
		-  runOutsideUI{}
		- runInsideUIAsync {}
	- A l'extérieur de l'UI nous allons :
		- Mettre en place une boucle permettant de toujours récupérer les messages.
    		- initialisez un message vide
    		- contrôlez si notre liste de message est vide ou non via le service
    		- Si la liste n'est pas vide :
    			- Et dans le cas ou elle n'est pas vide récupérez le message via le service
    			- Puis nous nous plaçons à l'intérieur de l'UI :
    				- et ajoutez le message au gridTchat du builder de la view en créant un label à la position (0, compteurY)
    			-  Puis incrémentez notre compteurY
    		- Si la liste est vide :
    			- Nous attendons 1s

## 5 Le passage de message 
- Maintenant que l'ensemble de votre MVC est fait, il vous faut passer le message de l'AppView vers la tchatView.
- Pour se faire : il vous faut allez dans l'AppController, une méthode du nom de launchTchat() vous y attend.
    - Dans cette méthode :
        - vérifiez si l'utilisateur a bien rentré un pseudo.
        - et créez le mvc du tchat en lui passant la variable.
- une fois la méthode faite, "bindez" le bouton "Valider" de l'AppView sur cette méthode

## C'est prêt !
- Si vous avez réussi cette étape vous pouvez tenter le lancement de votre application ! (lancez le serveur avant tout de même ça pourra vous éviter quelques soucis ;) 
- Vous pouvez même ouvrir plusieurs fois l'application de tchat pour simuler différents clients se connectant à notre serveur !
